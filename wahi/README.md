# WAHI - WebAssembly Host Interface

The WebAssembly Host Interface library is designed to provide a unified interface to WebAssembly *runtimes* for building runtime-generic libraries and ABIs. It is, in essence, an extremely enhanced version of the basic [C-ABI][wasm-c-abi], in a fashion modular to both the many features commonly present in the [WebAssembly standards][wasm-std] at compile time and with runtime conditionality.

## Overall Goals
This should be able to represent the methods, structure, features, etc. in a granular manner using rust's typesystem to ensure *compiletime correctness*. 

It should use the structure encoded by types and traits to maximise the modularity by which APIs depend on both the WASM runtime and other prerequisites, and should have the ability to encode this statically and optionally dynamically, and in a manner that can be reasonably implemented on modern WASM runtimes while also allowing for more performant or non-allocating cases (for example, avoiding `Arc` and allocation in general unless needed by the runtime, by being handle- and representation- agnostic as much as possible).

[wasm-c-abi]: https://webassembly.github.io/spec/core/appendix/embedding.html
[wasm-std]: https://webassembly.github.io/spec/


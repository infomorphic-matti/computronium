//! Structures and traits for the [types][wasm-types] for simple WASM modules.
//!
//! [wasm-types]: https://webassembly.github.io/spec/core/syntax/modules.html

use crate::{core::values::FuncType, repr::ModuleRepr};

/// Internal representation of type indexes and such for function type list.
pub trait TypesRepr {
    /// Index of a function type.
    ///
    /// See <https://webassembly.github.io/spec/core/syntax/modules.html#indices>.
    ///
    /// Lifetime is the [`Types`] lt.
    type TypeIndex<'functypes>;
}

/// Some collection of function types for a module with the current representation.
///
/// <https://webassembly.github.io/spec/core/syntax/modules.html#syntax-module>
///
/// Note that this is essentially dynamic in nature.
pub trait Types<R: TypesRepr> {
    /// Normalised form of the contained function type that is Owned and lasts at-least-as-long as
    /// this type.
    ///
    /// Similar types to [`Self::FuncTypeRef`] should also be usable by this if holding references. However if you are in
    /// an allocating environment it may be desirable to, for instance, take ownership of the
    /// function type.
    type FuncTypeOutlasting: FuncType;

    /// Normalised form of the contained function type returned when attempting to get the function
    /// type.
    ///
    /// See [`crate::core::values::DynFuncType`] or [`crate::core::values::BoxedFuncType`]
    /// for a sensible default.]
    type FuncTypeRef<'a>: FuncType
    where
        Self: 'a;

    /// Register the function signature with this collection of types.
    ///
    /// Note that returning [`None`] indicates that there was no more space available or some other
    /// limitation was met.
    fn accept_function_type(
        &mut self,
        signature: impl Into<Self::FuncTypeOutlasting>,
    ) -> Option<R::TypeIndex<'_>>;

    /// Resolve the given raw index into a key, if it is valid
    fn resolve_raw_function_type_index(&self, bare_key: u32) -> Option<R::TypeIndex<'_>>;

    /// Attempt to get the function type for the given key. In the case of a nonexistent key,
    /// return None.
    fn resolve_function_type(&self, key: R::TypeIndex<'_>) -> Option<Self::FuncTypeRef<'_>>;
}

// Copyright 2022 Matti Bryce <mattibryce at protonmail dot com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

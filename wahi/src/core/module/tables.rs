//! Structures and traits for the collection of tables declared by WASM modules
use super::limits;
use crate::{
    core::values::{RefType, TableType},
    repr::ModuleRepr,
};

/// Internal representation of table indices and such for table type (declaration) list.
pub trait TablesRepr {
    /// Index of a table
    ///
    /// See <https://webassembly.github.io/spec/core/syntax/modules.html#indices>
    ///
    /// Lifetime is the lifetime of the [`Tables`]
    type TableIndex<'tabletypes>;
}

/// Collection of tables needed/defined by a WASM module
///
/// <https://webassembly.github.io/spec/core/syntax/modules.html#modules>
///
/// Note that this is fundamentally dynamic in nature.
pub trait Tables<R: TablesRepr> {
    /// Attempt to register a new table declaration, returning None in case of failure.
    ///
    /// Note that unlike [`super::Types`], this takes things by value, because table specifications
    /// have a well known, fixed-size, and small representation.
    fn accept_table_type(&mut self, signature: TableType) -> Option<R::TableIndex<'_>>;

    /// Resolve the given raw index into a key if it's valid.
    fn resolve_raw_table_type_index(&self, bare_key: u32) -> Option<R::TableIndex<'_>>;

    /// Attempt to get the table type for the given key. In the case of a nonexistent key, return
    /// None.
    fn resolve_table_type(&self, key: R::TableIndex<'_>) -> Option<TableType>;
}

// Copyright 2022 Matti Bryce <mattibryce at protonmail dot com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

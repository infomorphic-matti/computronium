//! Encoding of simple WASM values and their types

#[derive(Copy, Clone, Hash, Eq, PartialEq, Debug)]
/// <https://webassembly.github.io/spec/core/syntax/types.html#syntax-numtype>
pub enum NumType {
    I32,
    I64,
    F32,
    F64,
}

/// <https://webassembly.github.io/spec/core/syntax/types.html#syntax-vectype>
#[derive(Copy, Clone, Hash, Eq, PartialEq, Debug)]
pub enum VecType {
    V128,
}

/// <https://webassembly.github.io/spec/core/syntax/types.html#reference-types>
#[derive(Copy, Clone, Hash, Eq, PartialEq, Debug)]
pub enum RefType {
    Func,
    Extern,
}

#[derive(Copy, Clone, Hash, Eq, PartialEq, Debug)]
/// <https://webassembly.github.io/spec/core/syntax/types.html#value-types>
pub enum ValueType {
    NumType(NumType),
    VecType(VecType),
    RefType(RefType),
}

impl From<NumType> for ValueType {
    #[inline]
    fn from(value: NumType) -> Self {
        Self::NumType(value)
    }
}

impl From<VecType> for ValueType {
    #[inline]
    fn from(value: VecType) -> Self {
        Self::VecType(value)
    }
}

impl From<RefType> for ValueType {
    #[inline]
    fn from(value: RefType) -> Self {
        Self::RefType(value)
    }
}

/// Unlike [`ValueType`], [`RefType`] and friends, [`ResultType`] represents an arbitrary
/// *sequence* of values (probably on the stack or similar). Because of storage issues, this is
/// instead constructed as a trait.
///
/// <https://webassembly.github.io/spec/core/syntax/types.html#result-types>
pub trait ResultType {
    /// Total number of values.
    fn length(&self) -> usize;

    /// Obtain the value at the given index. Should return [`None`] if out of range, and should
    /// not return None if in range (index < self.length())
    fn get(&self, index: usize) -> Option<ValueType>;
}

/// Results and parameters are the same type (that is, arbitrary tuple) of
pub use self::ResultType as ParamType;

impl<T: ?Sized + ResultType> ResultType for &T {
    #[inline(always)]
    fn length(&self) -> usize {
        (*self).length()
    }

    #[inline(always)]
    fn get(&self, index: usize) -> Option<ValueType> {
        (*self).get(index)
    }
}

impl ResultType for [ValueType] {
    #[inline(always)]
    fn length(&self) -> usize {
        self.len()
    }

    #[inline(always)]
    fn get(&self, index: usize) -> Option<ValueType> {
        self.get(index)
    }
}

#[cfg(feature = "alloc")]
impl<T: ?Sized + ResultType> ResultType for alloc::boxed::Box<T> {
    #[inline(always)]
    fn length(&self) -> usize {
        self.length()
    }

    #[inline(always)]
    fn get(&self, index: usize) -> Option<ValueType> {
        self.get(index)
    }
}

/// Like [`ResultType`], [`FuncType`] can have issues with storage. This  
///
/// <https://webassembly.github.io/spec/core/syntax/types.html#function-types>
pub trait FuncType {
    /// Parameters of function type.
    type ParamType<'a>: ParamType
    where
        Self: 'a;

    /// Results of function type.
    type ReturnType<'a>: ResultType
    where
        Self: 'a;

    fn parameters(&self) -> Self::ParamType<'_>;
    fn results(&self) -> Self::ReturnType<'_>;
}

/// Dynamic but non-allocating uniform function type storage, that uses Rust's standard dynamic
/// dispatch mechanism as a means of construction.
///
/// Requires that the result and param types are references.
pub struct DynFuncType<'o> {
    params: &'o dyn ParamType,
    results: &'o dyn ResultType,
}

impl<'o> FuncType for DynFuncType<'o> {
    type ParamType<'a> = &'a dyn ParamType;

    type ReturnType<'a> = &'a dyn ParamType;

    #[inline]
    fn parameters(&self) -> Self::ParamType<'_> {
        self.params
    }

    #[inline]
    fn results(&self) -> Self::ReturnType<'_> {
        self.result
    }
}

impl<'o, T: ?Sized + FuncType + 'o> From<&'o T> for DynFuncType<'o> {
    fn from(value: &'o T) -> Self {
        Self {
            params: value.parameters().into(),
            results: value.results().into(),
        }
    }
}

#[cfg(feature = "alloc")]
/// Like [`DynFuncType`] except it allocates on the heap to store the parameter and
/// result types in boxed slices.
///
/// In the case that the implementation of the
pub struct BoxedFuncType {
    params: alloc::boxed::Box<[ValueType]>,
    results: alloc::boxed::Box<[ValueType]>,
}

#[cfg(feature = "alloc")]
impl BoxedFuncType {
    /// Checks that the length of the provided function type has all-valid [`ValueType`] for
    /// every index up to it, returning None in case of otherwise.
    pub fn new_checked(curr: impl FuncType) -> Option<Self> {
        use alloc::vec::Vec;
        let (parameters, results) = (curr.parameters(), curr.results());

        let mut paramvec = Vec::with_capacity(parameters.length());
        let mut resultvec = Vec::with_capacity(results.length());

        // Get each in turn, checking is_some
        for idx in 0..parameters.length() {
            let next = parameters.get(idx)?;
            paramvec.push(next);
        }

        // Same as above
        for idx in 0..results.length() {
            let next = results.get(idx)?;
            resultvec.push(next);
        }

        // Box
        Some(Self {
            params: paramvec.into_boxed_slice(),
            results: resultvec.into_boxed_slice(),
        })
    }
}

#[cfg(feature = "alloc")]
impl FuncType for BoxedFuncType {
    type ParamType<'a> = &'a [ValueType] where Self: 'a;

    type ReturnType<'a>  = &'a [ValueType] where Self: 'a;

    #[inline]
    fn parameters(&self) -> Self::ParamType<'_> {
        &self.params
    }

    #[inline]
    fn results(&self) -> Self::ReturnType<'_> {
        &self.results
    }
}

/// Limits of a table or memory
///
/// <https://webassembly.github.io/spec/core/syntax/types.html#limits>
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Limits {
    pub min: u32,
    pub max: Option<u32>,
}

/// Individual sepcification of a single memory declaration/type - limits are in page size.
///
/// <https://webassembly.github.io/spec/core/syntax/types.html#memory-types>
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct MemType {
    pub limits: Limits,
}

/// The individual specification of a single table declaration/type - limits and all
///
/// <https://webassembly.github.io/spec/core/syntax/types.html#table-types>
#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub struct TableType {
    pub limits: Limits,
    pub reftype: RefType,
}

// Copyright 2022 Matti Bryce <mattibryce at protonmail dot com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![doc = include_str!("../README.md")]
// Documentation Magic for Features
#![cfg_attr(doc, feature(doc_auto_cfg))]
#![no_std]

#[cfg(feature = "alloc")]
extern crate alloc;

#[cfg(feature = "std")]
extern crate std;

/// Indicator traits for different representations
pub mod repr {
    /// Mostly contains index type representations and such.
    pub trait ModuleRepr {
        
    }
}

pub mod core;

pub use nclosure;
pub mod prelude {
    pub use nclosure::prelude::*;
}

// Copyright 2022 Matti Bryce <mattibryce at protonmail dot com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

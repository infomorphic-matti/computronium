# Computronium
This project provides an embeddable substrate to run arbitrary WASM such that it can be seamlessly moved from machine to machine - even automatically, in many cases - as well as persistent connections to running instances, nodes, etc.

That's the long term goal.

[cwasmhost]: https://webassembly.github.io/spec/core/appendix/embedding.html 
[wasmparser]: https://docs.rs/crate/wasmparser-nostd/0.91.0

## Components 
The goal of this project is to develop all the needed components for the aforementioned goal. To do this, we need, however, to develop a number of other things. For embeddability - and generic WASM packing/unpacking of state, it becomes necessary to develop a generic wasm host API, like [the simple C one][cwasmhost] but more sophisticated, rusty, and designed for efficiency and ease of implementation.

This is accomplished with several modules - currently some of them are not event started/

[component-model]: https://github.com/WebAssembly/component-model/blob/main/design/mvp/WIT.md

### WebAssembly Host Interface - `wahi`
This crate contains an abstraction over interfaces for a WASM host system, that should be adaptable for even very resource constrained systems. It is designed such that - hopefully - a runtime-agnostic WASM ABI translation layer - as defined in the [Component Model][component-model].

It uses [wasmparser-nostd][wasmparser] for parsing WASM files, which can then get fed to custom hosts. This will hopefully just be replaced by wasmparser once they allow for no_std. 

This crate is licensed under Apache-2.0 with the LLVM exception. 

### More
The names and information of other packages will appear at some point in the future.

